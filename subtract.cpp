// subtract.cpp

/**
 * C++ program to find the subtraction of two numbers.
 * Where, num1 = 96 and num2 = 45
 * Date: Sun, 5/2/23
 * 
 * Author: Ankur Saxena
 */

# include <iostream>
# include <stdlib.h>

using namespace std;

namespace findSubtraction {

    int subtract () {

        int num1 = 96;
        int num2 = 45;

        cout << "First number is : " << num1 << endl;
        cout << "Second number is : " << num2 << endl;

        // int result = num1 - num2;

        // calculating and printing subtraction
        cout << "The subtraction of " << num1 << " and " << num2 << " is : " 
        << (num1 - num2) << endl;

        return 0;
    }
}

int main (){

    system ("clear"); // use "cls" in Windows

    // calling namespace
    findSubtraction :: subtract ();

    return 0;
}
