// multistring1.cpp

# include <iostream>
# include <stdlib.h>

using namespace std;

void multistring (){

    string text = "Hi, C++ World!\n"
    "Welcome to the C++ language.\n"
    "C++ is fun and easy to learn language.\n"
    "Let us learn C++ language.\n";

    // printing string
    cout << text;
}

int main (){

    system ("clear"); // use "cls" in Windows machine
    
    // calling function
    multistring ();

    return 0;
}
