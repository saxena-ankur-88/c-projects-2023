// sum3.cpp

/**
 * C++ program to find the sum of two numbers using "cstdio" and "namespace".
 * Where, num1 = 20 and num2 = 45
 * Date: Sat, 04/02/23
 * 
 * Author: Ankur Saxena
 */

# include <cstdio>
# include <iostream>
# include <stdlib.h>

using namespace std;

// creating namespace
namespace addition1 {

    // creating function
    int sum (){

        int num1 = 20;
        int num2 = 45;

        cout << "First number is : " << num1 << endl;
        cout << "Second number is : " << num2 << endl;

        printf ("First number is : %d\n", num1);
        printf ("Second number is : %d\n", num2);

        // calculating sum
        int sum = num1 + num2;

        // printing sum
        printf ("The sum of %d and %d is : %d\n", num1, num2, sum);

        cout << "The sum of " << num1 << " and " << num2 << " is : " << sum
        << endl;

        return 0;
    }
}

// main function
int main (){

    system ("clear"); // use "cls" in Windows

    // calling namespace
    addition1 :: sum ();

    return 0;   
}
