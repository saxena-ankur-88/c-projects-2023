// C++ hello world program using functions

#include <iostream>
#include <stdlib.h>

using namespace std;

// creating function
void function1(){

    cout<<"Hi, Ankur Saxena!"<<endl;
    cout<<"Welcome to the C++ language."<<endl;
    cout<<"Let us learn C++ language."<<endl;
}

// main function
int main(){

    system ("clear");

    // calling function
    function1();

    return 0;
}
