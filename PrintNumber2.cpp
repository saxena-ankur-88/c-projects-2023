// PrintNumber2.cpp

// C++ program to print number 15 on the console screen using "cstdio" library and "printf" statement.
// Date: Tue, 04/04/23
// Author: Ankur Saxena

// using "cstdio" library

# include <cstdio>
# include <stdlib.h>

int printNumber (){

	int num = 15;

	printf ("The number is : %d\n", num);

	return 0;
}

// define main function
int main (){

	system ("clear");

	// call function
	printNumber ();

	return 0;
}
