//getcin1.cpp

# include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main () {

    system ("clear");

    cout << "Hi, Ankur Saxena!" << endl;

    // cin.get (); // used to pause the console screen

    // system ("pause"); // also used to pause the console screen // not recommended
    // this command is platform dependent. it only works in Windows machine

    return 0;
}
