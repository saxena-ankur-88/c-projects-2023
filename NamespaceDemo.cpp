// NamespaceDemo.cpp

// C++ program that demonstrate namespaces in C++
// Date: Sat, 08/04/23
// Author: Ankur Saxena

# include <iostream>
# include <stdlib.h>

using std :: cout;
using std :: endl;
using std :: string;

namespace intNamespace {

    int myIntNum = 42;
}

namespace stringNamespace {

    string myString = "Max!";
}

int main (){

    system ("clear");

    // calling namespaces
    cout <<"Integer value is : "<<intNamespace::myIntNum<<endl;
    cout <<"String value is : "<<stringNamespace::myString<<endl;

    return 0;
}
