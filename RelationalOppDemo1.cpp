// RelationalOppDemo1.cpp

// Simple C++ program to demonstrate relational operators.
// Date: Wed, 05/04/2023
// Author: Ankur Saxena

# include <iostream>
# include <stdlib.h>

using std :: cout;
using std :: endl;

int relationalOpp(){

    int num1 = 10;
    int num2 = 5;

    // demonstrating relational operators
    cout <<"First number is : "<<num1<<endl;
    cout <<"Second number is : "<<num2<<endl;

    cout <<"First number == Second number : "<<(num1 == num2)<<endl; // false
    cout <<"First number != Second number : "<<(num1 != num2)<<endl; // true
    cout <<"First number < Second number : "<<(num1 < num2)<<endl; // false
    cout <<"First number > Second number : "<<(num1 > num2)<<endl; // true
    cout <<"First number <= Second number : "<<(num1 <= num2)<<endl; // false
    cout <<"First number >= Second number : "<<(num1 >= num2)<<endl; // true

    return 0;
}

// define main function
int main (){

    system ("clear"); // used for clear the console screen
    // use "cls" in Windows

    // call the function
    relationalOpp();

    return 0;
}
