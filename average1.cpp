// average1.cpp

/**
 * C++ program to find the sum and average of two numbers on the console screen.
 * Where, num1 = 20 and num2 = 13
 * Date: Tue, 07/02/23
 * 
 * Author: Ankur Saxena
 */

# include <iostream>
# include <stdlib.h>

using namespace std;

// creating namespace
namespace avgSpace {

    // creating function
    int findAverage () {

        int num1 = 20, num2 = 13, sum;
        double average;

        // printing variable values
        cout << "First number is : " << num1 << endl;
        cout << "Second number is : " << num2 << endl;

        // calculating sum
        sum = num1 + num2;

        // calculating average
        average = (double) sum / 2.0;

        // printing sum and average
        cout << "Sum of the numbers is : " << sum << endl;
        cout << "Average of the numbers is : " << average << endl;

        return 0;
    }
}

// main function
int main () {

    system ("clear");

    // calling namespace
    avgSpace :: findAverage ();

    return 0;
}
