// multilinestring.cpp

/**
 * Multiline string program in c++ language.
 * In C++, we can create a multiline string using the escape sequence "\n". 
 * Date: Fri, 3/2/23
 * Author: Ankur Saxena
 * Platform Linux Ubuntu 22.04 (LTS)/x64/GNU GCC-g++ 12.1.0/VS Code 
 */

#include <iostream>
#include <stdlib.h>

using namespace std;

int main() {
    
    system ("clear"); // user "cls" in Windows

    string text = "This is a string\n"
                  "spread over multiple lines.\n"
                  "It can be useful for creating\n"
                  "readable code.";
    
    // printing string
    cout << text << endl;
  
  return 0;
}
