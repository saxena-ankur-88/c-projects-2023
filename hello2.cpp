/*
Write a C++ program to print "hello world" on the console screen using "cstdio" 
header file and "puts" command.
*/

#include<cstdio>
#include<stdlib.h>

using namespace std;

int main(){

    system ("clear"); // use "cls" in Windows

    puts ("Hi, C++ World!");
    puts ("Welcome to the C++ language...");
    printf ("Let us learn C++ language.\n");

    return 0;
}
