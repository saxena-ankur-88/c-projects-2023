// average2.cpp

/**
 * C++ program to find the sum and average of three numbers on the console screen.
 * Where, num1 = 25, num2 = 45 and num3 = 96
 * Date: Tue, 07/02/23
 * 
 * Author: Ankur Saxena
 */

# include <iostream>
# include <stdlib.h>

using namespace std;

// creating namespace
namespace avgSpace1 {

    // creating function
    int findAverage () {

        int num1 = 25, num2 = 45, num3 = 96, sum;
        double average;

        // printing variable values
        cout << "First number is : " << num1 << endl;
        cout << "Second number is : " << num2 << endl;
        cout << "Third number is : " << num3 << endl;

        // calculating sum
        sum = (num1 + num2) + num3;

        // calculating average
        average = (double) sum / 3.0;

        // printing sum and average
        cout << "The sum of " << num1 << ", " << num2 << " and " << num3 
        << " is : " << sum << endl;
        cout << "Average of the numbers is : " << average << endl;

        return 0;
    }
}

// main function
int main () {

    system ("clear");

    // calling namespace
    avgSpace1 :: findAverage ();

    return 0;
}
