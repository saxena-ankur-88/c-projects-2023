#include <iostream>
#include <stdlib.h>

using namespace std;

namespace printString{

    // creating function
    void function1(){

        string string1 = "Welcome to the C++ language.";
        string string2 = "Let us learn C++ language.";

        cout<<string1<<endl;
        cout<<string2<<endl;
    }
}

// main function
int main(){

    system ("cls"); // use "clear" in Linux or macOS

    puts ("Hi, Ankur Saxena!");
    
    // calling function
    printString::function1();

    return 0;
}
