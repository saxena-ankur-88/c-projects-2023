// string1.cpp

# include <iostream>
# include <stdlib.h>

using namespace std;

// creating namespace
namespace firstString {

    // creating function
    void marsRover () {

        string name = "Rover";
        int age = 10;
        
        // printing result

        cout << "Happy birthday " << name << "!" << endl;
        cout << "You are now " << age << " years old." << endl;
    }
}

// main function
int main () {

    system ("clear"); // use "cls" in Windows machine

    // calling namespace
    firstString :: marsRover ();

    return 0;
}
