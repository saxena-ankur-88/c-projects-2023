// demonamespace2.cpp

#include <cstdio>
#include <stdlib.h>

using namespace std;

// first namespace
namespace firstSpace {

    void function1 (){

        puts ("Indide first space....");
    }
}

// second namespace
namespace secondSpace {

    void function2 (){

        puts ("Inside second space....");
    }
}

// main function
int main (){

    system ("clear");

    // calls function from first namespace
    firstSpace :: function1();

    // calls function from secons namespace
    secondSpace :: function2();

    return 0;
}
