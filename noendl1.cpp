// C++ program without using the endl manipulator
// Date: Sat, 14/01/23

// noendl.cpp

#include <iostream>
#include <stdlib.h>

using namespace std;

int main(){

    system ("clear"); // use "cls" in Windows to clear the console

    cout<<"Ram is a good boy.";
    cout<<"Mohan is also a good boy.";

    return 0;
}
