// variabledemo1.cpp
// C++ Variable demo program.

#include <iostream>
#include <stdlib.h>

using namespace std;

int main(){

    system ("clear");

    int myNum = 15; // integer variable
    double myFloatNum = 5.99; // real or floatimg-point variable
    char myLetter = 'A'; // character variable
    string myString = "Hello!"; // string or text variable
    bool myBooleanValue = true; // boolean variable (true or false)

    // printing variables
    cout<<myNum<<endl;
    cout<<myFloatNum<<endl;
    cout<<myLetter<<endl;
    cout<<myString<<endl;
    cout<<myBooleanValue<<endl;

    return 0;
}
