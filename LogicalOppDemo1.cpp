// LogicalOppDemo1.cpp

// Simple C++ program that demonstrate the use of logical operators.
// Date: Thu, 06/04/23
// Author: Ankur Saxena

#include <iostream>
#include <stdlib.h>

using std :: cout;
using std :: endl;

int logicOpp(){

    int num1 = 10;
    int num2 = 5;
    int num3 = 0;

    // print variable values
    cout<<"First number (num1) is : "<<num1<<endl;
    cout<<"Second number (num2) is : "<<num2<<endl;
    cout<<"Third number (num3) is : "<<num3<<endl;

    // demonstrating logical operators
    cout<<"num1 > num2 && num1 > num3 is : "<< (num1 > num2 && num1 > num3)<<endl; // true
    cout<<"num1 > num2 || num1 > num3 is : "<< (num1 > num2 || num1 > num3)<<endl; // true
    cout<<"!num3 is : "<< (!num3)<<endl; // true

    return 0;
}

// define main function
int main(){

    system ("clear");

    // call the function
    logicOpp();

    return 0;
}
