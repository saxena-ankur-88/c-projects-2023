// constdemo1.cpp
// C++ demo program to demonstrate C++ Constants.

#include <iostream>
#include <stdlib.h>

using namespace std;

int main(){

    system ("clear");

    const int intValue = 10; // integer constant
    const float floatValue = 5.16; // real or floating-point constant
    const char charValue = 'A'; // character constant
    const string stringValue = "ABC"; // string constant

    // printing constant variable values

    cout<<"Integer Constant : "<<intValue<<endl;
    cout<<"Real or Floating-point Constant : "<<floatValue<<endl;
    cout<<"Character Constant : "<<charValue<<endl;
    cout<<"String Constant : "<<stringValue<<endl;

    return 0;
}
