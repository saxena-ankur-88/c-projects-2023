// IncDecDemo.cpp

// Simple C++ program that demonstrates the increment and decrement operator.
// Date: Fri, 07/04/203
// Author: Ankur Saxena

#include <iostream>
#include <stdlib.h>

using std :: cout;
using std :: endl;

int incOpp(){

	int num = 19;

	cout <<"Value before increment = " <<num<<endl;

	int num1 = ++num; // pre-fix increment
	// num1 = 1 + 19 = 20

	cout <<"Value after increment = " <<num1<<endl;

	return 0;
}

int decOpp(){

	int num = 10;

	cout <<"Value before decrement = " <<num<<endl;

	int num1 = --num; // pre-fix increment
	// num1 = 1 - 10 = 9

	cout <<"Value after decrement = " <<num1<<endl;

	return 0;
}

int main(){

	system ("clear");

	// calling functions
	incOpp();
	decOpp();

	return 0;
}
