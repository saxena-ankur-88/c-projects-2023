// C++ program implementing the endl manipulator
// Date: Sat, 14/01/23

// endldemo.cpp

#include <iostream>
#include <stdlib.h>

using namespace std;

int main(){

    system ("clear");

    cout<<"Ram is a good boy."<<endl;
    cout<<"Mohan is also a good boy."<<endl;

    return 0;
}
