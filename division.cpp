// division.cpp

/**
 * C++ program to find the division of two numbers.
 * Where, num1 = 51 and num2 = 8
 * Date: Mon, 06/02/23
 * 
 * Author: Ankur Saxena
 */

# include <iostream>
# include <stdlib.h>

using namespace std;

// creating namespace
namespace divisionSpace {

    // creating function
    int findDivision () {

        int num1 = 51;
        int num2 = 8;

        // printing variable values
        cout << "First number is : " << num1 << endl;
        cout << "Second number is : " << num2 << endl;

        // calculating division
        double result = (double) num1 / (double) num2; // type casting or conversion

        // printing division result
        cout << "The division of " << num1 << " and " << num2 << " is : " 
        << result << endl;

        return 0;
    }
}

// main function
int main () {

    system ("clear"); // use "cls" in Windows

    // calling namespace
    divisionSpace :: findDivision ();

    return 0;
}
