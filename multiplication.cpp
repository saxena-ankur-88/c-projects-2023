// multiplication.cpp

/**
 * C++ program to find the multiplication of two numbers.
 * Where, num1 = 12 and num2 = 15
 * Date: Sun, 5/2/23
 * 
 * Author: Ankur Saxena
 */

# include <iostream>
# include <stdlib.h>

using namespace std;

namespace multiplication {

    int findMultiplication () {

        int num1 = 12, num2 = 15;

        cout << "First number is : " << num1 << endl;
        cout << "Second number is : " << num2 << endl;

        // int result = num1 * num2;

        // calculating and printing multiplication
        cout << "The multiplication of " << num1 << " and " << num2 << " is : " 
        << (num1 * num2) << endl;

        return 0;
    }
}

int main (){

    system ("clear"); // use "cls" in Windows

    // calling namespace
    multiplication :: findMultiplication ();

    return 0;
}
