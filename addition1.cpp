// addition1.cpp

/**
 * C++ program to find the addition of three numbers.
 * Where, num1 = 25, num2 = 30 and num3 = 46
 * Date: Mon, 06/02/23
 * 
 * Author: Ankur Saxena
 */

# include <iostream>
# include <stdlib.h>

using namespace std;

// creating namespace
namespace sumSpace {

    // creating function
    int findSum () {

        int num1 = 25, num2 = 30, num3 = 46;

        // printing variable values
        cout << "First number is : " << num1 << endl;
        cout << "Second number is : " << num2 << endl;
        cout << "Third number is : " << num3 << endl;

        // calculating sum and printing result
        cout << "The sum of " << num1 << ", " << num2 << " and " << num3 
        << " is : " << (num1 + num2) + num3 << endl;

        return 0;
    }
}

// main function
int main () {

    system ("clear");

    sumSpace :: findSum ();

    return 0;
}
